"use strict";

$(document).ready(function () {
  $(".phone").inputmask({
    mask: "+7(999)-999-99-99",
    showMaskOnHover: false
  });
  $(".open-modal").on("click", function (e) {
    e.preventDefault();
    $(".t-modal").toggle();
  });
  $(".t-modal__centered").on("click", function (e) {
    e.preventDefault();

    if (e.target.className === "t-modal__centered") {
      $(".t-modal").hide();
    }
  });
  $(".t-modal__close").on("click", function (e) {
    e.preventDefault();
    $(".t-modal").hide();
  });
  $(".t-header__toggle").on("click", function (e) {
    e.preventDefault();
    $(".t-header__nav").slideToggle("fast");
    $(".t-header__right").slideToggle("fast");
    $(".t-top").slideToggle("fast");
  });
  $(".t-tabs__header li").click(function () {
    var tab_id = $(this).attr("data-tab");
    $(".t-tabs__header li").removeClass("current");
    $(".t-tabs__content").removeClass("current");
    $(this).addClass("current");
    $("#" + tab_id).addClass("current");
  });
  $(".t-goods-card").on("click", function (e) {
    e.preventDefault();
    $(".t-goods-card").removeClass("t-goods-card_active");
    $(this).addClass("t-goods-card_active");
    $(".t-goods__subtitle").html($(this).data("value"));
    $(".table-goods").hide();
    $("#".concat($(this).data("toggle"))).show();
  });
  new Swiper(".t-about__swiper", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    pagination: {
      el: ".swiper-pagination"
    },
    loop: true,
    autoplay: {
      delay: 4000
    }
  });
  new Swiper(".t-news", {
    slidesPerView: 1,
    slidesPerColumn: 5,
    slidesPerColumnFill: "row",
    spaceBetween: 30,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    pagination: {
      el: ".swiper-pagination",
      type: "fraction"
    },
    breakpoints: {
      1200: {
        spaceBetween: 60
      }
    }
  });
  new Swiper(".t-another__cards", {
    slidesPerView: 1,
    spaceBetween: 30,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper(".t-long__cards", {
    slidesPerView: 1,
    spaceBetween: 5,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  });
  $("body").addClass("nohover");
  $("td, th").attr("tabindex", "1").on("touchstart", function () {
    $(this).focus();
  });
});
//# sourceMappingURL=main.js.map